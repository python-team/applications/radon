Source: radon
Section: devel
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alexandre Detiste <tchet@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all:any,
 python3-docutils,
 python3-sphinx,
 libjs-mathjax,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/rubik/radon
Vcs-Git: https://salsa.debian.org/python-team/packages/radon.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/radon

Package: radon
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python tool to compute code metrics (Python3)
 Radon is a Python tool which computes various code metrics.
 Supported metrics are:
 .
 raw metrics: SLOC, comment lines, blank lines, &c.
 Cyclomatic Complexity (i.e. McCabe’s Complexity)
 Halstead metrics (all of them)
 the Maintainability Index (a Visual Studio metric)
 .
 Radon can be used either from the command line or
 programmatically through its API.

Package: python-radon-doc
Architecture: all
Section: doc
Depends: libjs-mathjax, ${sphinxdoc:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: Python tool to compute code metrics (common documentation)
 Radon is a Python tool which computes various code metrics.
 Supported metrics are:
 .
 raw metrics: SLOC, comment lines, blank lines, &c.
 Cyclomatic Complexity (i.e. McCabe’s Complexity)
 Halstead metrics (all of them)
 the Maintainability Index (a Visual Studio metric)
 .
 Radon can be used either from the command line or
 programmatically through its API.
 .
 This is the common documentation package.
